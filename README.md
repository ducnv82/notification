### Challenge: Multi API notification service
The objective is to build a set of different standalone services which are able to create a notification by a template and sent the generated notification to an imaginary mail-service.
We assume that the usage of that mail service is paid by request. Therefore, in case of sending newsletters, the service should only be called with a batch of at least 10 messages.
There are 3 services that has to be implemented:

* User-Service is able to return [user-data](users.json) by:
    * user-id
    * all users

* Template-Service is able to:
    * return a specifc template by id
    * return a specifc template by key

* Notification-Service can be triggered in two use-cases:

    * use-case one: assume a new user is created
        * user data is loaded by user-service
        * [welcome template](templateWelcome.txt) is loaded by template service and enriched with user-data
        * once template is enriched with data it is sent directly to imaginary mail-service

    * use-case two: asume newsletter hast to be sent:
        * [newsletter](templateNewsletter.txt) is loaded by template service and enriched with user-data of all subscribed users
        * enriched newsletter messages are sent as a batch of at least 10 to imaginary mail service


#### Constraints
* you provide a straight forward documentation how to build and run the services
* your services are covered with tests
* free choice of following programming languages: Scala, Java, Python

#### Bonus
* all services have to run as standalone applications
* you let your services run within a container based environment (Docker, Kubernetes)
* you provide a documentation of your services API endpoints
* user and template service are connected to database an get data from there instead of provided files

------------------------------------------------------------


# Microservices architecture:

1.  template-service is a spring boot web app running at the port 8084 to serve all functionality related to template. REST API(s) are in TemplateController.
2.  user-service is a spring boot web app running at the port 8085 to serve all functionality related to user. REST API(s) are in UserController.
3.  notification-service is a spring boot web app running at the port 8086 to serve all functionality related to notification. REST API(s) are in NotificationController.

*   notification-core is a reusable module used by all microservices
*   notification-parent is the parent project contains only a pom.xml file to manage dependencies, configurations for all modules  

# Prerequisite

1. Java: Install latest Oracle JDK 8, download at http://www.oracle.com/technetwork/java/javase/downloads/index.html
2. Install latest apache maven or at least version 3.6.0

# Build and run the application

1.  Build: In the folder `notification-parent` run the command `mvn clean install` to build the project and install dependencies to local maven repository
2.  Run the app `template-service`: in the folder _template-service_  run the command `mvn spring-boot:run`. Then the app will be available at http://localhost:8084/api/templates
3.  Run the app `user-service`: in the folder _user-service_  run the command `mvn spring-boot:run`. Then the app will be available at http://localhost:8085/api/users
4.  Run the app `notification-service`: in the folder _notification-service_  run the command `mvn spring-boot:run`. Then the app will be available at http://localhost:8086/api/notifications
  
Or you can import the projects into an IDE like (Eclipse with spring tool suite) and run 3 projects in classes _TemplateServiceApplication_, _UserServiceApplication_, _NotificationServiceApplication_ as `Spring Boot App`  
Running NotificationServiceTest in IDE: You must specify a JVM argument to jmockit.jar, my PC is: `-javaagent:C:/Users/Duc/.m2/repository/org/jmockit/jmockit/1.45/jmockit-1.45.jar`

# Test the application
We can use [Postman](https://www.getpostman.com/ "Postman") to test REST APIs, import the Postman collection [Notification.postman_collection.json](Notification.postman_collection.json). 
