package com.mycompany.notification.template.service;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

import org.springframework.http.ResponseEntity;

public interface TemplateService {

    ResponseEntity<Object> findById(@Valid @NotBlank String id);

    ResponseEntity<Object> findByKey(@Valid @NotBlank String key);
}
