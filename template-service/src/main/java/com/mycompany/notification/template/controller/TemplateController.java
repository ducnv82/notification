package com.mycompany.notification.template.controller;

import com.mycompany.notification.template.service.TemplateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/templates/")
public class TemplateController {

    @Autowired
    private TemplateService templateService;

    @GetMapping("{id}")
    public ResponseEntity<Object> findTemplateById(@PathVariable final String id) {
        return templateService.findById(id);
    }

    @GetMapping(path = "find-by-key")
    public ResponseEntity<Object> findTemplateByKey(@RequestParam final String key) {
        return templateService.findByKey(key);
    }
}
