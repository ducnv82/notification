package com.mycompany.notification.template.service;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

import com.mycompany.notification.model.Template;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

@Service
@Validated
public class TemplateServiceImpl implements TemplateService {

    private Map<String, Template> templates;

    public TemplateServiceImpl() throws IOException, URISyntaxException {
        templates = new HashMap<>();

        final Template welcomeTemplate = new Template();
        welcomeTemplate.setId("1");
        welcomeTemplate.setKey("welcome");
        welcomeTemplate.setContent(
                new String(Files.readAllBytes(Paths.get(new ClassPathResource("templateWelcome.txt").getURI())),
                        StandardCharsets.UTF_8));

        templates.put(welcomeTemplate.getKey(), welcomeTemplate);

        final Template newsletterTemplate = new Template();
        newsletterTemplate.setId("2");
        newsletterTemplate.setKey("newsletter");
        newsletterTemplate.setContent(
                new String(Files.readAllBytes(Paths.get(new ClassPathResource("templateNewsletter.txt").getURI())),
                        StandardCharsets.UTF_8));

        templates.put(newsletterTemplate.getKey(), newsletterTemplate);
    }

    @Override
    public ResponseEntity<Object> findById(@Valid @NotBlank final String id) {
        for (final Template template : templates.values()) {
            if (template.getId().equals(id)) {
                return ResponseEntity.ok(template);
            }
        }
        return ResponseEntity.notFound().build();
    }

    @Override
    public ResponseEntity<Object> findByKey(@Valid @NotBlank final String key) {
        final Template template = templates.get(key);
        return template == null ? ResponseEntity.notFound().build() : ResponseEntity.ok(template);
    }
}
