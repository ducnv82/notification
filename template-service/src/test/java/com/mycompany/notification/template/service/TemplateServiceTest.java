package com.mycompany.notification.template.service;

import java.io.IOException;
import java.lang.reflect.Field;
import java.net.URISyntaxException;

import com.google.common.collect.ImmutableMap;
import com.mycompany.notification.model.Template;
import com.mycompany.notification.util.Constants;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.springframework.util.ReflectionUtils.findField;
import static org.springframework.util.ReflectionUtils.makeAccessible;
import static org.springframework.util.ReflectionUtils.setField;

public class TemplateServiceTest {

    private Template mockTemplate;
    private TemplateService templateService;

    private Template createTemplate() {
        final Template template = new Template();
        template.setId("1");
        template.setKey(Constants.KEY_WELCOME);
        template.setContent("welcome content");
        return template;
    }

    @Before
    public void setUp() throws IOException, URISyntaxException {
        mockTemplate = createTemplate();

        templateService = new TemplateServiceImpl();
        final Field fieldTemplates = findField(TemplateServiceImpl.class, "templates");
        makeAccessible(fieldTemplates);
        setField(fieldTemplates, templateService, ImmutableMap.of(mockTemplate.getKey(), mockTemplate));
    }

    @Test
    public void testFindById() {
        final Object body = templateService.findById(mockTemplate.getId()).getBody();
        assertNotNull(body);

        final Template template = (Template) body;
        assertEquals(template, mockTemplate);
    }

    @Test
    public void testFindById_notFound() {
        final Object body = templateService.findById("2").getBody();
        assertNull(body);
    }

    @Test
    public void testFindByKey() {
        final Object body = templateService.findByKey(mockTemplate.getKey()).getBody();
        assertNotNull(body);

        final Template template = (Template) body;
        assertEquals(template, mockTemplate);
    }

    @Test
    public void testFindByKey_notFound() {
        final Object body = templateService.findByKey("2").getBody();
        assertNull(body);
    }
}
