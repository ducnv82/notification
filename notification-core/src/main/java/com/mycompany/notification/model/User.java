package com.mycompany.notification.model;

public class User extends BaseModel {

    private String salutation;
    private String name;

    public String getName() {
        return name;
    }

    public String getSalutation() {
        return salutation;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public void setSalutation(final String salutation) {
        this.salutation = salutation;
    }
}
