package com.mycompany.notification.model;

public abstract class BaseModel {

    protected String id;
    protected long version;

    public String getId() {
        return id;
    }

    public void setId(final String id) {
        this.id = id;
    }
}
