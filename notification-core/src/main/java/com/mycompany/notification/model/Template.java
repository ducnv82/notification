package com.mycompany.notification.model;

public class Template extends BaseModel {

    private String key;
    private String content;

    public String getContent() {
        return content;
    }

    public String getKey() {
        return key;
    }

    public void setContent(final String content) {
        this.content = content;
    }

    public void setKey(final String key) {
        this.key = key;
    }
}
