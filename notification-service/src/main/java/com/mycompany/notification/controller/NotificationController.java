package com.mycompany.notification.controller;

import com.mycompany.notification.service.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/notifications/")
public class NotificationController {

    @Autowired
    private NotificationService notificationService;

    @PostMapping("{userId}/notify-when-user-is-created")
    public ResponseEntity<Object> notifyWhenUserIsCreated(@PathVariable final String userId) {
        return notificationService.notifyWhenUserIsCreated(userId);
    }

    @PostMapping("send-newsletter")
    public ResponseEntity<Object> sendNewsletter() {
        return notificationService.sendNewsletter();
    }
}
