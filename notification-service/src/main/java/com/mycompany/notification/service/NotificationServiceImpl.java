package com.mycompany.notification.service;

import java.util.Iterator;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

import com.google.common.collect.Iterables;
import com.mycompany.notification.model.Template;
import com.mycompany.notification.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

@Service
@Validated
public class NotificationServiceImpl implements NotificationService {

    private static final Logger LOGGER = LoggerFactory.getLogger(NotificationServiceImpl.class);

    @Autowired
    private RestTemplate restTemplate;

    @Override
    public ResponseEntity<Object> notifyWhenUserIsCreated(@Valid @NotBlank final String userId) {
        User user;
        try {
            user = restTemplate.getForObject("http://localhost:8085/api/users/{id}", User.class, userId);
        } catch (final HttpClientErrorException e) {
            LOGGER.error("Exception when get user", e);
            return new ResponseEntity<>(e.getResponseBodyAsString(), e.getStatusCode());
        }

        LOGGER.debug("user: {}", user);

        Template template;
        try {
            template = restTemplate.getForObject("http://localhost:8084/api/templates/find-by-key?key=welcome",
                    Template.class);
        } catch (final HttpClientErrorException e) {
            LOGGER.error("Exception when get template", e);
            return new ResponseEntity<>(e.getResponseBodyAsString(), e.getStatusCode());
        }

        final String templateContent = template.getContent();
        LOGGER.debug("template content: {}", templateContent);

        final String enrichedContent = templateContent.replace("{{user.salutation}}", user.getSalutation())
                .replace("{{user.name}}", user.getName()).replace("{{user.id}}", user.getId());
        // template is enriched with data, it is sent directly to imaginary mail-service. Logging
        // here
        LOGGER.debug("template is enriched with data will be sent directly to imaginary mail-service: {}",
                enrichedContent);

        return ResponseEntity.ok("Success");
    }

    @Override
    public ResponseEntity<Object> sendNewsletter() {
        Template template;
        try {
            template = restTemplate.getForObject("http://localhost:8084/api/templates/find-by-key?key=newsletter",
                    Template.class);
        } catch (final HttpClientErrorException e) {
            LOGGER.error("Exception when get template", e);
            return new ResponseEntity<>(e.getResponseBodyAsString(), e.getStatusCode());
        }

        final String templateContent = template.getContent();
        LOGGER.debug("template content: {}", templateContent);

        List<User> users;
        try {
            users = restTemplate.exchange("http://localhost:8085/api/users", HttpMethod.GET, null,
                    new ParameterizedTypeReference<List<User>>() {/* EMPTY */}).getBody();
        } catch (final HttpClientErrorException e) {
            LOGGER.error("Exception when get all users", e);
            return new ResponseEntity<>(e.getResponseBodyAsString(), e.getStatusCode());
        }

        LOGGER.debug("users size: {}", users.size());
        final Iterator<List<User>> userIterator = Iterables.partition(users, BATCH_SIZE).iterator();
        int count = 0;
        while (userIterator.hasNext()) {
            final int batch = count + 1;
            final List<User> userPartition = userIterator.next();
            final int userPartitionSize = userPartition.size();

            // enriched newsletter messages are sent as a batch of 10 to imaginary mail service.
            // Logging here
            LOGGER.info("Sending user to imaginary mail service for batch {} with size {}", batch, userPartitionSize);
            for (final User user : userPartition) {
                final String enrichedContent = templateContent.replace("{{user.salutation}}", user.getSalutation())
                        .replace("{{user.name}}", user.getName()).replace("{{user.id}}", user.getId());
                LOGGER.debug("template is enriched with data will be sent directly to imaginary mail-service: {}",
                        enrichedContent);
            }
            count++;
        }
        return ResponseEntity.ok("Success");
    }
}
