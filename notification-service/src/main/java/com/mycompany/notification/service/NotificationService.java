package com.mycompany.notification.service;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

import org.springframework.http.ResponseEntity;

public interface NotificationService {

    int BATCH_SIZE = 10;
    String USER_SERVICE_URL = "http://localhost:8085/api/users";
    String TEMPLATE_SERVICE_URL = "http://localhost:8084/api/templates";

    ResponseEntity<Object> notifyWhenUserIsCreated(@Valid @NotBlank String userId);

    ResponseEntity<Object> sendNewsletter();
}
