package com.mycompany.notification.service;

import java.util.Arrays;
import java.util.List;

import com.mycompany.notification.model.Template;
import com.mycompany.notification.model.User;
import com.mycompany.notification.util.Constants;
import mockit.Expectations;
import mockit.Injectable;
import mockit.Tested;
import org.junit.Test;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class NotificationServiceTest {

    @Tested
    private NotificationServiceImpl notificationService;

    @Injectable
    private RestTemplate restTemplate;

    private Template createTemplate() {
        final Template template = new Template();
        template.setId("1");
        template.setKey(Constants.KEY_WELCOME);
        template.setContent("welcome content");
        return template;
    }

    private User createUser() {
        final User user = new User();
        user.setId("1");
        user.setName("User 1");
        user.setSalutation("Mr.");
        return user;
    }

    @Test
    public void testNotifyWhenUserIsCreated() {
        final User user = createUser();
        final Template template = createTemplate();

        new Expectations() {
            {
                restTemplate.getForObject("http://localhost:8085/api/users/{id}", User.class, user.getId());
                result = user;

                restTemplate.getForObject(
                        "http://localhost:8084/api/templates/find-by-key?key=" + Constants.KEY_WELCOME, Template.class);
                result = template;
            }
        };

        final ResponseEntity<Object> response = notificationService.notifyWhenUserIsCreated(user.getId());
        assertNotNull(response);
        assertEquals(200, response.getStatusCodeValue());
        assertEquals("Success", response.getBody());
    }

    @Test
    public void testNotifyWhenUserIsCreated_templateNotFound() {
        final User user = createUser();

        new Expectations() {
            {
                restTemplate.getForObject("http://localhost:8085/api/users/{id}", User.class, user.getId());
                result = user;

                restTemplate.getForObject(
                        "http://localhost:8084/api/templates/find-by-key?key=" + Constants.KEY_WELCOME, Template.class);
                result = new HttpClientErrorException(HttpStatus.NOT_FOUND);
            }
        };

        final ResponseEntity<Object> response = notificationService.notifyWhenUserIsCreated(user.getId());
        assertNotNull(response);
        assertEquals(404, response.getStatusCodeValue());
    }

    @Test
    public void testNotifyWhenUserIsCreated_userNotFound() {
        final String userId = "notFound";
        new Expectations() {
            {
                restTemplate.getForObject("http://localhost:8085/api/users/{id}", User.class, userId);
                result = new HttpClientErrorException(HttpStatus.NOT_FOUND);
            }
        };

        final ResponseEntity<Object> response = notificationService.notifyWhenUserIsCreated(userId);
        assertNotNull(response);
        assertEquals(404, response.getStatusCodeValue());
    }

    @Test
    public void testSendNewsletter() {
        final Template template = createTemplate();
        final User user = createUser();

        new Expectations() {
            {
                restTemplate.getForObject(
                        "http://localhost:8084/api/templates/find-by-key?key=" + Constants.KEY_NEWSLETTER,
                        Template.class);
                result = template;

                restTemplate.exchange("http://localhost:8085/api/users", HttpMethod.GET, null,
                        new ParameterizedTypeReference<List<User>>() {/* EMPTY */}).getBody();
                result = Arrays.asList(user);
            }
        };

        final ResponseEntity<Object> response = notificationService.sendNewsletter();
        assertNotNull(response);
        assertEquals(200, response.getStatusCodeValue());
        assertEquals("Success", response.getBody());
    }

    @Test
    public void testSendNewsletter_templateNotFound() {
        new Expectations() {
            {
                restTemplate.getForObject(
                        "http://localhost:8084/api/templates/find-by-key?key=" + Constants.KEY_NEWSLETTER,
                        Template.class);
                result = new HttpClientErrorException(HttpStatus.NOT_FOUND);
            }
        };

        final ResponseEntity<Object> response = notificationService.sendNewsletter();
        assertNotNull(response);
        assertEquals(404, response.getStatusCodeValue());
    }

    @Test
    public void testSendNewsletter_findUsersInternalException() {
        final Template template = createTemplate();

        new Expectations() {
            {
                restTemplate.getForObject(
                        "http://localhost:8084/api/templates/find-by-key?key=" + Constants.KEY_NEWSLETTER,
                        Template.class);
                result = template;

                restTemplate.exchange("http://localhost:8085/api/users", HttpMethod.GET, null,
                        new ParameterizedTypeReference<List<User>>() {/* EMPTY */}).getBody();
                result = new HttpClientErrorException(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        };

        final ResponseEntity<Object> response = notificationService.sendNewsletter();
        assertNotNull(response);
        assertEquals(500, response.getStatusCodeValue());
    }
}
