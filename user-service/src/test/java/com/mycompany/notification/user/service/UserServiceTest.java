package com.mycompany.notification.user.service;

import java.lang.reflect.Field;
import java.util.List;

import com.google.common.collect.ImmutableMap;
import com.mycompany.notification.model.User;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.springframework.util.ReflectionUtils.findField;
import static org.springframework.util.ReflectionUtils.makeAccessible;
import static org.springframework.util.ReflectionUtils.setField;

public class UserServiceTest {

    private User mockUser;
    private UserService userService;

    private User createUser() {
        final User user = new User();
        user.setId("1");
        user.setName("User 1");
        user.setSalutation("Mr.");
        return user;
    }

    @Before
    public void setUp() {
        mockUser = createUser();

        userService = new UserServiceImpl();
        final Field fieldUsers = findField(UserServiceImpl.class, "users");
        makeAccessible(fieldUsers);
        setField(fieldUsers, userService, ImmutableMap.of(mockUser.getId(), mockUser));
    }

    @Test
    public void testFindAll() {
        final List<User> users = userService.findAll();
        assertTrue(users.size() == 1);
        assertEquals(users.get(0), mockUser);
    }

    @Test
    public void testFindById() {
        final Object body = userService.findById(mockUser.getId()).getBody();
        assertNotNull(body);

        final User user = (User) body;
        assertEquals(user, mockUser);
    }

    @Test
    public void testFindById_notFound() {
        final Object body = userService.findById("2").getBody();
        assertNull(body);
    }

}
