package com.mycompany.notification.user.service;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

import com.mycompany.notification.model.User;
import org.springframework.http.ResponseEntity;

public interface UserService {

    List<User> findAll();

    ResponseEntity<Object> findById(@Valid @NotBlank String id);
}
