package com.mycompany.notification.user.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

import com.mycompany.notification.model.User;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

@Service
@Validated
public class UserServiceImpl implements UserService {

    private Map<String, User> users;

    public UserServiceImpl() {
        users = new HashMap<>();
        for (int i = 0; i < 100; i++) {
            final String id = String.valueOf(i);
            final User user = new User();
            if (i % 2 == 0) {
                user.setSalutation("Mr.");
                user.setName(id + "-even");
            } else {
                user.setSalutation("Ms.");
                user.setName(id + "-odd");
            }
            user.setId(id);
            users.put(id, user);
        }
    }

    @Override
    public List<User> findAll() {
        return new ArrayList<>(users.values());
    }

    @Override
    public ResponseEntity<Object> findById(@Valid @NotBlank final String id) {
        final User user = users.get(id);
        return user == null ? ResponseEntity.notFound().build() : ResponseEntity.ok(user);
    }
}
